<?php
/*
  The following types for parameters can now be enforced.
  Whether coercively or structly
  strings | integers | floating point numbers | bool
*/

declare(strict_types=1);
/*
function getInt($input) :int{
    return $input;
}

function getFloat($input) :float{
    return $input;
}

function getString($input) :string{
    return $input;
}

function getBool($input) :bool{
    return $input;
}

$input = 2;
$input = 2.3; // will return error for int since we are in "declare(strict_types=1);"

var_dump(getInt($input));
var_dump(getFloat($input));
var_dump(getString($input));
var_dump(getBool($input));
*/

function createAddress(int $number,
string $street, string $apt = null){
  if($apt){
    return sprintf('%d %s, #%s', $number, $street, $apt);
  } else {
    return sprintf('%d %s', $number, $street);
  }
}

echo createAddress(15, 'Main st', '3');
