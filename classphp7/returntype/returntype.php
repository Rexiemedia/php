<?php
/*
Return type are necessary when a function is to be 
inherited and the name of the child function must 
match the name of the subclass function
*/
interface A {
    static function make(): A;
}

class B implements A{
    static function make(): B { //this will leade to error 
        return new B(); 
    } 
}

//implements or extends is the same 
class B implements A{
    static function make(): A { //this is right
        return new B(); 
    } 
}

function sum($num1, $num2){
    return $num1 + $num2;
}
var_dump(sum(2,3)); // will return int(5)

// But if we want to return a type we have to add the type
function sum($num1, $num2): int{
    return $num1 + $num2;
}
var_dump(sum(2,3)); // will return int(5)

// But if we want to return a int which normally is a float
function sum($num1, $num2): int{
    return $num1 + $num2;
}
var_dump(sum(2,3.5)); // will return int(5)

// But if we dont return a type it will be automatic float.
function sum($num1, $num2){
    return $num1 + $num2;
}
var_dump(sum(2,3.5)); // will return float(5.5)

// check type function
function checkType($input){
    if(is_float($input)){
        echo 'FLOAT';
    } else if (is_int($input)){
        echo 'INTEGER';
    }else if (is_double($input)){
        echo 'DOUBLE';
    }else if (is_string($input)){
        echo 'STRING';
    }else if (is_bool($input)){
        echo 'BOOLEAN';
    }else if (is_array($input)){
        echo 'ARRAY';
    }else if (is_object($input)){
        echo 'OBJECT';
    }else {
        echo 'UNKNOWN';
    }
}

// function foo(){
//     return 'Hello';
//     return ['2', '3'];
// }

// and we can also specify the return type like this
function foo(): array{
    return ['2', '3'];
}


checkType(foo()); //will return STRING, ARRAY as the case maybe

// if inheriting a returned type both super and subclass must match
interface A {
    static function make(): int;
}

class B implements A{
    static function make(): int{  
        return new B(); 
    } 
}