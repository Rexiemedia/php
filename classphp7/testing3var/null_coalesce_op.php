<?php
// $user = $username ?? 'Guest'; //this return Guest

// $username = 'Rexie';
// $user = $username ?? 'Guest';
 //this return Rex since user value is avalable

// if(empty($username)){
//     $user = 'Guest';
// }else{
//     $user = $username;
// }

// This is not the best because if user name doesn't exist it will blow up 
// but this $user = $username ?? 'Guest'; will not
// $user = ($username ? $username : 'Guest');

// we can also test for  3 variable
// $user = $username ?? $myUseer ?? 'Guest';
// echo $user;


// $_GET username
$username = $_GET['username'] ?? 'No User Specified';
// http://localhost/php/classphp7/null_coalesce_op.php?username=Rexieee
// echo $username;

$username = $_POST['username'] ?? 'Guest';
echo " Hello $username";
?>

<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<!-- This $_SERVER 'PHP_SELF'] is because we are running this code inside this page -->
    <input type="text" name="username">
    <input type="submit" value="Submit">
</form>