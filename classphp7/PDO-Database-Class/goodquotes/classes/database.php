<?php
  class Database{
    private $db_host = 'localhost';
    private $db_user = 'root';
    private $db_pass='123456';
    private $db_name = 'goodquotes';

    protected $dbh;
    protected $stmt;

    public function __construct(){
      $this->dbh = new PDO("mysql:host=".$this->db_host.";dbname=".$this->db_name,$this->db_user, $this->db_pass);
    }

    public function query($query){
      $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null){
      if(is_null($type)){
        switch(true){
          case is_int($value):
            $type = PDO::PARAM_INT;
            break;
          case is_bool($value):
            $type = PDO::PARAM_BOOL;
            break;
          case is_null($value):
            $type = PDO::PARAM_NULL;
            break;
          default:
            $type = PDO::PARAM_STR;
        }
      }
      $this->stmt->bindValue($param, $value, $type);
    }

    public function execute(){
      $this->stmt->execute();
      return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function lastInsertId(){
      return $this->dbh->lastInsertId();
    }

    public function single(){
      $this->execute();
      return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
  }
