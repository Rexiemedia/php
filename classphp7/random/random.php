<?php
/*
$rand = random_int(100, 999999);
var_dump($rand);

$bytes = random_bytes(5);
echo bin2hex($bytes);
*/

function randomToken($length = 32){
  if(!isset($length) || intval($length) <= 8){
    $length = 32;
  }

  if(function_exists('random_bytes')){
    return bin2hex(random_bytes($length));
  }

  if(function_exists(mcrypt_create_iv)){
    return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
  }

}

function salt(){
  return substr(strtr(base64_encode(hex2bin(randomToken(32))),'+', '.'),0, 44);
}

//echo (randomToken());
echo salt();
