<?php
class User{
  public function __construct(){
    echo 'User Construct...<br>';
  }

  public function __destruct(){
    echo '...User Destruct <br>';
  }
}

function sum(int $num1, int $num2){
  return $num1 + $num2;
}

try{
  $user = new User();
  //echo sum('Test',2);
  //$result = eval("var_dup(1);");
  require('./required.php');
} catch(Exception $e) { //php5 exception way Exception this will stop the rest of the script
  echo $e->getMassage();
} catch(Error $e){ // while php7 is Error will throw the error but continue with other part of the script
  echo '<strong>'.get_class($e).' on line '.$e->getLine().' of '.$e->getFile().': </strong>'.$e->getMessage().'<br>';
}
