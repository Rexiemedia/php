<?php
echo "<h1>\u{2190}</h1>"; // returns ← symbol

$currency = "\u{20A4}"; 
echo "<h1>$currency 5000</h1>"; //₤ 5000 with space
echo '<h1>'.$currency.'5000 </h1>'; //₤5000 best option

echo "<h1>\u{00A7}</h1>";
echo "<h1>\u{A7}</h1>"; // same as above since the 00 are in front it can be removed


echo "<h1>\u{202E}Reverse Text</h1>"; //Reverse Text