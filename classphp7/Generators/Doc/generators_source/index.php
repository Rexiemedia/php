<?php
/*
function myCounter(){
  echo 'Counter starting...<br>';
  for($i = 0; $i <= 10; $i++){
    yield 'Yielded '.$i.'<br>';
  }
  echo 'Counter stopped...';
}

foreach(myCounter() as $c){
  echo $c;
}
*/

// // returns the value only
// function readLines($filename){
//   $file = fopen($filename, 'r');
//   while(($line = fgets($file)) !== false){
//     yield  $line;
//   }
//   fclose($file);
// }

// foreach(readLines('test.txt') as  $line){
//   echo  $line.'<br>';
// }



// // returns the value and the keys
// function readLines($filename){
//   $file = fopen($filename, 'r');
//   $i = 0;
//   while(($line = fgets($file)) !== false){
//     $key = $i;
//     yield $key => $line;
//     $i++;
//   }
//   fclose($file);
// }

// foreach(readLines('test.txt') as $key => $line){
//   echo $key .' - '. $line.'<br>';
// }

// php 7 phytures that allows to return value from a gen

// function myCounter(){
//   echo 'Counter starting...<br>';
//   $total = 0;
//   for($i = 0; $i <= 10; $i++){
//     yield 'Yielded '.$i.'<br>';
//     $total += 1;
//   }
//   return $total;
// }

// $counter = myCounter();

// foreach(myCounter() as $c){
//   echo $c;
// }

// echo 'Total - '.$counter->getReturn(); 
// // this returned 11 since $1 <= 10 + $total += 1;

// returns the value and the keys
function readLines($filename){
  $file = fopen($filename, r);
  $i = 0;
  while(($line = fgets($file)) !== false){
    $key = $i;
    yield $key => $line;
    $i++;

  }
  fclose($file);
  return '<br>'.$i.' Lines read from '.$filename;
}

$lines = readLines('test.txt');

foreach($lines as $key => $line){
  echo $key .' - '. $line.'<br>';
}

echo $lines->getReturn();

