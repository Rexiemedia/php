<?php

 // echo "It Works";
  // defining a constant
  // define('URL', 'https://rexiemedia.firebaseapp.com');
  // echo URL;

  // $myArr1 = [22, 35, 44, 65, 100];
  // $myArr2 = array('Hi', 'Hello', 'Good morning');
  // Associate array
  //  $myArr3 = array('Jon' => 23, 'Ben' => 44, 'Matt' => 33);
  // to print array use print_r
  // print_r($myArr1); //= Array ( [0] => 22 [1] => 35 [2] => 44 [3] => 65 [4] => 100 )
  // echo $myArr2 [1]; //Hello
  // echo $myArr3 ['Jon']; //23
  
  // Concatenations
  // $age = 23;
  // echo "I am $age years old";

    // OR single quote and concat the variable 
  // echo 'I am '.$age.' years old';

// Functions
//   function sayHello($words){
//   echo $words;
// }
// sayHello('Good Morning');

// Loops
// for($i = 0; $i < 10; $i++){
//   echo 'Number '.$i. '<br>';
// }

// $i = 0;

// while($i < 10){
//   echo 'Number '.$i. '<br>';
//   $i++;
// }

// $numbers = [43, 54, 33, 21];

// foreach($numbers as $number){
//   echo $number. '<br>';
// }

/*
Space ship operator
Return 0 - if the values are equal
return 1 - if value on the left is greater
return -1 if the value on the right is greater
*/

// Numbers
// $a = 25;
// $b - 20;
// echo $a <=> $b; // return 1

// $a = 25;
// $b = 30;
// echo $a <=> $b; // return -1

// Floats
// $a = 1.5;
// $b = 2;
// echo $a <=> $b; // return -1

// $a = 2.5;
// $b = 2;
// echo $a <=> $b; // return 1

// Strings
// $a = "aa";
// $b = "bb";
// echo $a <=> $b; // return -1

// $a = "cc";
// $b = "bb";
// echo $a <=> $b; // return 1

// Arrays
// $a = [1,2,3];
// $b = [1,2,3,4];
// echo $a <=> $b; // return -1

// Objects
// $a = (object) ["a" => "d"];
// $b = (object) ["a" => "c"];
// echo $a <=> $b; // return 1

// $a = (object) ["a" => "b"];
// $b = (object) ["a" => "c"];
// echo $a <=> $b; // return -1


if(($handle = fopen("people.csv", "r")) !== FALSE){
  while(($row = fgetcsv($handle, 1000, ",")) !== FALSE){
    $data[] = $row;
  }
  fclose($handle);
}

usort($data, function($left, $right){
  return $left[1] <=> $right[1];
});

foreach($data as $person){
  echo $person[1].' - '.$person[5].'<br>';
}
