<?php
  session_start();

  include_once 'config.php';
  include_once './classes/database.php';
  include_once './classes/quote.php';

   try{
     $quotesObj = new Quote();
     $quote = $quotesObj->getSingle($_GET['id']);
  }catch(Throwable $e){
      error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
      echo '<div class="alert alert-danger">Error Please contact Support</div>';

  } 

  if(isset($_POST['submit'])){
    $id = $_GET['id'];
    $text = $_POST['text']?: null;
    $creator = $_POST['creator']?: 'Guess';

        try{
            $quotesObj = new Quote();
            $quotesObj = update( $id, $text, $creator);
        }catch(Throwable $e){
            echo '<div class="alert alert-danger">Error Please contact Support</div>';

        } 
  }
 
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Quotes</title>
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.php">Home</a></li>
            <li role="presentation" class="active"><a href="#">New Quote</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">GoodQuotes | Adding a Quote</h3>
      </div>
      <div class="row marketing">
        <div class="col-lg-12">
        <h2 class="pageheader">Edit Quote</h2>
             <form method="POST" action="edit.php?id=<?php echo $_GET['id'] ?>">
            <div class="form-group">
                <label> Quote Text</lable>
                <input type="text" class="form-control" name="text" 
                   value="<?php echo $quote['text']?>"  placeholder="Enter your quote">
            </div>
             <div class="form-group">
                <label>Creator / Author</lable>
                <input type="text" class="form-control" name="creator" 
                   value="<?php echo $quote['creator']?>" placeholder="Enter your Name">
            </div>
            <button type="submit" name="submit" class="btn btn-default">Submit</button>
        </form>
         </div>
      </div>

      <footer class="footer">
        <p>&copy; 2019 GoodQuotes, Inc.</p>
      </footer>

    </div> 
    <!-- /container -->
  </body>
</html>
