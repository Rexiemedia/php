<?php

    class Database{

        private $db_host = 'localhost';        
        private $db_user = 'rexie';
        private $db_pass = 'Alpha';
        private $db_name = 'goodquotes';

        protected $dbh;
        protected $stmt;

        public function __constructor(){
            try{
                $this->dbh = new PDO('mysql:host=localhost;port=3306;dbname=loginsystem', 
   'rexie', 'Alpha');
                // $this->dbh = new PDO('mysql:host=localhost;port=3306;dbname=goodquotes', 'rexie', 'Alpha');
            }catch(PDOException $e){
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';
            }
            
        }

        public function query($query){
            try{
                 $this->stmt = $this->dbh->prepare($query);
            }catch(Throwable $e){
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';
            }
        }

        public function bind($param, $value, $type = null){
            if(is_null($type)){
                switch(true){
                    case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;

                    case bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;

                    case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;

                    default:
                    $type = PDO::PARAM_STR;
                }
            }
            $this->stmt->bindValue($param, $value, $type);
        }

        public function execute(){
             try{
            $this->stmt->execute();
            }catch(Throwable $e){
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';
            }
        }

        public function resultSet(){
            try{
                $this->execute();           
                return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
            }catch(Throwable $e){
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';
            }
        }

        public function lastInsertId(){
            return $this->dbh->lastInsertId();
        }

         public function single(){
           try{
                $this->execute();           
                return $this->stmt->fetch(PDO::FETCH_ASSOC);
            }catch(Throwable $e){
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';
            }
        }
    }