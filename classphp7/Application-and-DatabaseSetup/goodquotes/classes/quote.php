<?php
include_once 'config.php';
include_once './classes/database.php';

    class Quote extends Database{
        public function index(){
            try{
                $this->query('SELECT * FROM quotes ORDER BY create_date DESC');
            // using generator to get the resultSet
                $i = 0;
                while($rows = $this->resultSet()){
                    if($i < count($rows)){
                        yield $rows[$i];
                        $i++;
                    }else {
                        return count($rows). 'Quotes Listed';
                    }
                }
            }catch(Throwable $e){ //Due to the fact that Error won't work in php5 we use Throwable
                error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                $_SESSION["error"] = '<div class="alert alert-danger">Error Please contact Support</div>';
            }
        }

        public function add(string $text, string $creator){
            try{
                $this->query('INSERT INTO quotes(text, creator) VALUES(:text, :creator)');
                $this->bind(':text', $text);
                $this->bind(':text', $creator);
                $this->execute();
            }catch(Throwable $e){
                 error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';

            } 

            if($this->lastInsertId()){
                header('Location: index.php');
            }
        }

        public function getSingle($id){
            try{
                $this->query('SELECT * FROM quotes WHERE id = :id');
                $this->bind(':id', $id);
                $row = $this->single();
            }catch(Throwable $e){
                 error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';

            } 
        }

        public function update(int $id, string $text, string $creator){
            try{
                $this->query('UPDATE  quotes SET text= :text, creator = :creator WHERE id = :id');
                $this->bind(':id', $id);
                $this->bind(':text', $text);
                $this->bind(':creator', $creator);
                $this->execute();

            }catch(Throwable $e){
                 error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
                echo '<div class="alert alert-danger">Error Please contact Support</div>';

            }
            header('Location: index.php');
        }
    }