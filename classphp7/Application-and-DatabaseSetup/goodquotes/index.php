<?php
  session_start();

  include_once 'config.php';
  include_once './classes/database.php';
  include_once './classes/quote.php';

  try{
     $quotesObj = new Quote();
     $quotes = $quotesObj->index();
  }catch(Throwable $e){
      error_log ('Connection Error'.$e->getLine().$e->getFile().$e->getMessage());
      echo '<div class="alert alert-danger">Error Please contact Support</div>';

  } 
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>GoodQuotes</title>
    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="index.php">Home</a></li>
            <li role="presentation"><a href="new.php">New Quote</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">GoodQuotes</h3>
      </div>

      <div class="jumbotron">
        <h1>Got A Quote?</h1>
        <p class="lead">Store your favorite quotes here to access and read them daily and better your life</p>
        <p><a class="btn btn-lg btn-success" href="new.php" role="button">Add Quote Now</a></p>
      </div>

      <div class="row marketing">
        <div class="col-lg-12">
          <?php 
            foreach($quotes as $q){
              echo '<h3><a href"edit.php?id="'.$q['id'].'</a>'.$q['text'].'</h3>';
              echo '<p>'.$q['creator'].'</p>';
            }
            ?>
         </div>
      </div>

      <footer class="footer">
        <p>&copy; 2019 GoodQuotes, Inc.</p>
      </footer>

    </div> 
    <!-- /container -->
  </body>
</html>
