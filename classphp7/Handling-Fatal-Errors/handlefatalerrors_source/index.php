<?php
class User{
  public function __construct(){
    echo 'User Construct...<br>';
  }

  public function __destruct(){
    echo '...User Destruct <br>';
  }
}

function run($object){
  $object->hello();
}

try{
  $user = new User();
  run(null);
} catch(Error $e) { 
  //instead Exception we use Error else it will throw a Fatal error which will deprive the destructor from running
  error_log('<strong>Error on line:'.$e->getLine().' in '.$e->getFile().' </strong>'.$e->getMessage());
  // get error line numb, get file, and get the error message logged in error log file
  echo '<strong>Error on line:'.$e->getLine().' in '.$e->getFile().' </strong>'.$e->getMessage().'<br>';
  // best practic echo '<strong>Error on process contact support</strong>';
} finally{
  echo 'Finally Ran...<br>';
}
