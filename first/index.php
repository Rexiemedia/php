<!-- display_errors =  on

; Even when display_errors is on, errors that occur during PHP's startup
; sequence are not displayed.  It's strongly recommended to keep
; display_startup_errors off, except for when debugging.
display_startup_errors = on -->

 <!DOCTYPE html>
 <html>
 <head>
	 <meta charset="utf-8" />
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <title>Rex Severance PHP</title>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
	 <script src="main.js"></script>
 </head>
 <body>
	 
 </body>
 </html>
 </html>
<h1>Rex Severance Request / Response</h1>
<p>
<pre>
<?php
$name = '"Rex Severance"';
	echo("The SHA256 hash of $name is ");
	print hash('sha256', 'Rex Severance ');
?>
<br>
ASCII ART:

    ***********
    **       **
    **
    **
    **
    **       **
    ***********
	
</pre>
<a href="http://localhost/first/check.php">Click here to check the error setting</a> <br>
<a href="http://localhost/first/fail.php">Click here to cause a traceback</a>
<?php
	// echo"Hi there\n";
// $x = 6 * 7;
// echo "The answer is ",$x," what was the question\n";
?>
</p>