<?php
require_once "pdo.php";
session_start();

// Demand a GET parameter
if ( ! isset($_SESSION["email"]) || strlen($_SESSION["email"]) < 1  ) {
    die('ACCESS DENIED');
}

// If the user requested logout go back to index.php
if ( isset($_POST['logout']) ) {
    header('Location: index.php');
    return;
}

// retrieving the data
$stmt = $pdo->query("SELECT make, year, mileage, model, autos_id FROM autos");
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
<title>Rex Ovie Otavotoma</title>
<style>
td {
    padding: 20px ;
    border: 1px solid black;
    text-align: center;
    width: 160px;
    height: 30px;
}
a{
    padding: 15px;
}
</style>
<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Tracking Autos for <br>
<?php
if ( isset($_SESSION["email"]) ) {
    echo htmlentities($_SESSION["email"]);
}
?>
</h1>
 
<?php
if ( isset($_SESSION['success']) ) {
  echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
  unset($_SESSION['success']);
}
if ( isset($_SESSION["success"]) ) {
        echo('<p style="color:green">'.htmlentities($_SESSION["success"])."</p>\n");
        unset($_SESSION["success"]);
}  
?>
 <table border="1">
    <?php
        foreach ( $rows as $row ) {
            echo "<tr><td>";
            echo($row['make']);
            echo("</td><td>");
            echo($row['model']);
            echo("</td><td>");
            echo($row['year']);
            echo("</td><td>");
            echo($row['mileage']);
            echo("</td><td>");
            echo('<a href="edit.php?autos_id='.$row['autos_id'].'">Edit</a> / ');
            echo('<a href="delete.php?autos_id='.$row['autos_id'].'">Delete</a>');
            echo("</td>");
            echo("</tr>\n");
        }
    ?>
 </table>

<br>
 <p><a href="add.php">Add New Entry</a> | <a href="logout.php" name="logout">Logout</a><p>
</p>
</div>
</body>
</html>
