<?php // Do not put any HTML above this line
session_start();
if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to index.php
    header("Location: index.php");
    return;
}

$salt = 'XyZzy12*_';
$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';  // Pw is php123

$email =($_POST['email']);

// Check to see if we have some POST data, if we do process it
if ( isset($_POST['email']) && isset($_POST['pass']) ) {
    unset($_SESSION["email"]);  // Logout current user
    if ( strlen($_POST['email']) < 1 || strlen($_POST['pass']) < 1 ) {
        $_SESSION["error"] = "User name and password are required.";
        header('Location: login.php');
        error_log("input errors ".$_SESSION["error"]);
        return;
    } 
    elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
          $_SESSION["error"] = "Email must have an at-sign (@)";
          header('Location: login.php');
          error_log("Email error ".$failure);
          return;
    }else {
        $check = hash('md5', $salt.$_POST['pass']);
        if ( $check == $stored_hash ) {
            $_SESSION["email"] = $_POST["email"];
            $_SESSION["success"] = "Logged in.";
            header("Location: view.php?name=".urlencode($_SESSION["email"]));
            // header("Location: index.php?name=".urlencode($_SESSION["email"]));
            error_log("Login success ".$_SESSION["email"]);
            return;
        } else {
             $_SESSION["error"] = "Incorrect password";
             header('Location: login.php');
             error_log("Login fail ".$_SESSION["email"]." $check");
             return;
        }
    }
}

// Fall through into the View
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Rex Ovie Otavotoma</title>
</head>
<body>
<div class="container">
<div><h1>Login with your Details</h1></div>
 
 <?php
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="POST">
<label for="nam">User Name</label>
<input type="text" name="email" id="nam" placeholder="Enter Email"><br/>
<label for="id_1723">Password</label>
<input type="text" name="pass"><br/>
<input type="submit" value="Log In">
<input type="submit" name="cancel" value="Cancel">
</form>
<p>
For a password hint, view source and find a password hint
in the HTML comments.
<!-- Hint: The password is the four character sound a cat
makes (all lower case) followed by 123. -->
</p>
</div>
</body>
