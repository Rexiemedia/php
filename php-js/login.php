<?php // Do not put any HTML above this line
require_once "pdo.php";
require_once "utility.php";
session_start();

if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to index.php
    header("Location: index.php");
    return;
}

$salt = 'XyZzy12*_';

$email =($_POST['email']);

// Check to see if we have some POST data, if we do process it
if ( isset($_POST['email']) && isset($_POST['pass']) ) {
    unset($_SESSION["email"]);  // Logout current user
    if ( strlen($_POST['email']) < 1 || strlen($_POST['pass']) < 1 ) {
        $_SESSION["error"] = "User name and password are required.";
        header('Location: login.php');
        error_log("input errors ".$_SESSION["error"]);
        return;
    } 
    elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
          $_SESSION["error"] = "Email must have an at-sign (@)";
          header('Location: login.php');
          error_log("Email error ".$failure);
          return;
    }else {
        $check = hash('md5', $salt.$_POST['pass']);

        $stmt = $pdo->prepare('SELECT user_id, name FROM users
        WHERE email = :em AND password = :pw');
        $stmt->execute(array( ':em' => $_POST['email'], ':pw' => $check));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( $row !== false ) {
            $_SESSION['name'] = $row['name'];
            $_SESSION['user_id'] = $row['user_id'];
            // Redirect the browser to index.php
            header("Location: index.php?name=".urlencode($_SESSION["name"]));
            header("Location: index.php");
            return;
        } else {
             $_SESSION["error"] = "Incorrect password";
             header('Location: login.php');
             error_log("Login fail ".$_SESSION["email"]." $check");
             return;
        }
    }
}

// Fall through into the View
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Rex Ovie Otavotoma</title>
</head>
<body>
<div class="container">
<div>
    <h1>Please Log In</h1>
</div>
 
 <?php
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="POST">
<label for="nam">User Name</label>
<input type="text" name="email" id="nam" placeholder="Enter Email"><br/>
<label for="id_1723">Password</label>
<input type="password" name="pass" id="id_1723"><br/>
<input type="submit" onclick="return doValidate();" value="Log In">
<input type="submit" name="cancel" value="Cancel">
</form>
<p>
For a password hint, view source and find a password hint
in the HTML comments.
<!-- Hint: The password is the four character sound a cat
makes (all lower case) followed by 123. -->
</p>
</div>
<script>
    function doValidate() {
            console.log('Validating...');
        try {
                pw = document.getElementById('id_1723').value;
                console.log("Validating pw="+pw);
            if (pw == null || pw == "") {
                alert("Both fields must be filled out");
                return false;
            }
            return true;
            } 
        catch(e) {
            return false;
            }
            return false;
    }
</script>
</body>
