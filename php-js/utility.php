<?php
    require_once "pdo.php";
    session_start();

    function checkAccess(){
        // Demand a GET parameter
        if ( ! isset($_SESSION["user_id"]) || strlen($_SESSION["user_id"]) < 1  ) {
            die('ACCESS DENIED');
            return;
        }
    }

     function flashErr(){
        // Flash pattern
        if ( isset($_SESSION['error']) ) {
            echo '<p style="color:red">'.htmlentities($_SESSION['error'])."</p>\n";
            unset($_SESSION['error']);
        }   
    }

    function flashSuccess(){
        if ( isset($_SESSION['success']) ) {
            echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
            unset($_SESSION['success']);
        } 
    }

    function validateProfile(){
        if ( strlen($_POST['first_name']) < 1 || strlen($_POST['last_name']) < 1
            || strlen($_POST['email'])<1 || strlen($_POST['headline'])<1 || isset($_POST['summary'])< 1) {
            return 'All fields are required';
        }
        if ( strpos($_POST['email'],'@') === false ) {
            return 'Email address must contain @';
        }
    }

    function validatePos() {
        for($i=1; $i<=9; $i++) {
            if ( ! isset($_POST['year'.$i]) ) continue;
            if ( ! isset($_POST['desc'.$i]) ) continue;
                $year = $_POST['year'.$i];
                $desc = $_POST['desc'.$i];
            if ( strlen($year) == 0 || strlen($desc) == 0 ) {
                return "All fields are required";
            }
            if ( ! is_numeric($year) ) {
                return "Position year must be numeric";
            }
        }
        return true;
    }

    function validateEdu() {
        for($i=1; $i<=9; $i++) {
            if ( ! isset($_POST['edu_year'.$i]) ) continue;
            if ( ! isset($_POST['edu_school'.$i]) ) continue;
                $edu_year = $_POST['edu_year'.$i];
                $edu_school = $_POST['edu_school'.$i];
            if ( strlen($edu_year) == 0 || strlen($edu_school) == 0 ) {
                return "All fields are required";
            }
            if ( ! is_numeric($edu_year) ) {
                return "Education year must be numeric";
            }
        }
        return true;
    }

    function loadPos($pdo, $profile_id){
        $stmt = $pdo->query('SELECT * FROM position WHERE profile_id = '.$profile_id.' ORDER BY rank');
        $positions = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $positions;
    }

    function loadEdu($pdo, $profile_id){
        $stmt = $pdo->prepare('SELECT * FROM Education JOIN Institution ON 
        Education.institution_id = Institution.institution_id WHERE 
        profile_id = '.$profile_id.' ORDER BY rank');

        $stmt->execute(array(':prof' => $profile_id));
        $schools = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $schools;
    }

    function insertPositions($pdo, $profile_id){
        $rank = 1;
        for($i=1; $i<=9; $i++) {
            if ( ! isset($_POST['year'.$i]) ) continue;
            if ( ! isset($_POST['desc'.$i]) ) continue;

            $year = htmlentities($_POST['year'.$i]);
            $desc = htmlentities($_POST['desc'.$i]);
            $stmt = $pdo->prepare('INSERT INTO Position
                (profile_id, rank, year, description)
                VALUES ( :pid, :rank, :year, :desc)');

            $stmt->execute(array(
            ':pid' => $_REQUEST['profile_id'],
            ':rank' => $rank,
            ':year' => $year,
            ':desc' => $desc)
            );

            $rank++;
        }
    }

    // Inserting Education
    function insetEducation($pdo, $profile_id){
        $rank = 1;
        for($i=1; $i<=9; $i++) {
            if ( ! isset($_POST['edu_year'.$i]) ) continue;
            if ( ! isset($_POST['edu_school'.$i]) ) continue;

            $year = htmlentities($_POST['edu_year'.$i]);
            $school = htmlentities($_POST['edu_school'.$i]);

            // Lookup the school if it is there.
            $institution_id = false;
            $stmt = $pdo->prepare('SELECT institution_id FROM Institution WHERE name = :name');
            $stmt->execute(array(':name' => $school)); // $school is coming from $school = $_POST['edu_school'.$i];
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if($row !== false) {
                $institution_id = $row['institution_id'];
            }

            // if the institution not already exist in the database we insert it
            if ($institution_id === false){
                $stmt = $pdo->prepare('INSERT INTO Institution (name) VALUES ( :name)');
                $stmt->execute(array(':name' => $school));
                // PDO has the posibility of returning the last inserted object 
                // id which will be used to insert into education database as it's forign KEY
                $institution_id = $pdo->lastInsertId();
            }
            
            // Inserting into Education table
            $stmt = $pdo->prepare('INSERT INTO Education (profile_id, institution_id, rank, year )
                VALUES ( :pid, :iid, :rank, :year)');
            $stmt->execute(array(
            ':pid' => $_REQUEST['profile_id'], //profile Id is coming from $_REQUEST['profile_id'] = $pdo->lastInsertId();
            ':iid' => $institution_id, // institution_id is coming from $institution_id = $pdo->lastInsertId();
            ':rank' => $rank, // rank is coming from  $rank = 1; and  $rank++;
            ':year' => $year) //coming from $year = $_POST['edu_year'.$i];
            );

            $rank++;
        }
    }