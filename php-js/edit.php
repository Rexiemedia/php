
<?php
require_once "pdo.php";
require_once "utility.php";
session_start();
checkAccess();

if ( isset($_POST['cancel']) ) {
    header('Location: index.php');
    return;
}

// Guardian: first_name sure that user_id is present
if ( ! isset($_GET['profile_id']) ) {
  $_SESSION['error'] = "Missing profile_id";
  header('Location: index.php');
  return;
}

// Loead up the profile in question
$stmt = $pdo->prepare('SELECT * FROM profile WHERE profile_id = :prof AND user_id = :uid');
$stmt->execute(array(':prof' => $_REQUEST['profile_id'], ':uid' => $_SESSION['user_id']));
$profile = $stmt ->fetch(PDO::FETCH_ASSOC);
if($profile === false){
    $_SESSION['error'] = "Could not load profile";
    header( 'Location: index.php' ) ;
    return;
}


// Handle the incoming data
if ( isset($_POST['first_name']) && isset($_POST['last_name'])
     && isset($_POST['email']) && isset($_POST['headline'])
     && isset($_REQUEST['profile_id'])&& isset($_SESSION['user_id'])) {
    
        // Data validation
    $msg = validateProfile();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
         header("Location: edit.php?profile_id=".$_REQUEST["profile_id"]);
        return;
    }

    //  Validate Education
    $msg = validateEdu();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
        header("Location: edit.php?profile_id=".$_REQUEST["profile_id"]);
        return;
    } 

    //  Validate Positions
    $msg = validatePos();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
          header("Location: edit.php?profile_id=".$_REQUEST["profile_id"]);
        return;
    } 
      
    $stmt = $pdo->prepare('UPDATE profile SET first_name = :first_name,
            last_name = :last_name, email = :email,
            headline = :headline, summary = :summary
            WHERE profile_id = :profile_id AND user_id =:uid');
    $stmt->execute(array(
        ':profile_id' => $_REQUEST['profile_id'],
        ':uid' => $_SESSION['user_id'],
        ':first_name' => $_POST['first_name'],
        ':last_name' => $_POST['last_name'],
        ':email' => $_POST['email'],
        ':headline' => $_POST['headline'],
        ':summary' => $_POST['summary']));
      
    $stmt = $pdo->prepare('DELETE FROM Position WHERE profile_id=:pid');
    $stmt->execute(array( ':pid' => $_REQUEST['profile_id']));
    
    //Adding positions from utility 
    insertPositions($pdo, $_REQUEST['profile_id']);
    
    // Deleting profile id from education will delete institution since its cascaded
    $stmt = $pdo->prepare('DELETE FROM Education WHERE profile_id=:pid');
    $stmt->execute(array( ':pid' => $_REQUEST['profile_id']));

    //Adding Education from utility 
    insetEducation($pdo, $_REQUEST['profile_id']);

    $_SESSION['success'] = 'Profile updated';
    header( 'Location: index.php' ) ;
    return;
}


    $fn = htmlentities($profile['first_name']);
    $ln = htmlentities($profile['last_name']);
    $em = htmlentities($profile['email']);
    $hl = htmlentities($profile['headline']);
    $sm = htmlentities($profile['summary']);
    $profile_id = $_REQUEST['profile_id'];

    $positions =  loadPos($pdo, $profile_id);
    $schools =  loadEdu($pdo, $profile_id);
?>
<html>
<head>
  <title>Rex Ovie Otavotoma</title>
  <?php require_once "bootstrap.php"; ?>
</head>
  <body>
    <div class = "container">
    <p><h1>Editing Profile</h1></p>
    <?php  flashErr(); ?>
    <form method="post">
    <input type="hidden" name="profile_id" value="<?= $profile_id ?>">
    <p>First Name
    <input type="text" name="first_name" value="<?= $fn ?>"></p>
    <p>Last Name
    <input type="text" name="last_name" value="<?= $ln ?>"></p>
    <p>email
    <input type="text" name="email" value="<?= $em ?>"></p>
    <p>headline <br>
    <input type="text" name="headline" value="<?= $hl ?>"></p>
    <p>Summary: <br>
    <textarea name="summary" rows="10" cols="80"><?= $sm ?>
    </textarea>
    <?php
                 
        echo '<p>Education: <input type="submit" id="addEdu" value="+"></p>';
        echo '<div id="edu_fields">';
        if(count($schools) > 0){
            foreach($schools as $school){
                echo '<div id="edu'.htmlentities($school['rank']).'">';
                echo '<p>Year: <input type="text" name="edu_year'.htmlentities($school['rank']).'" value="'.$school['year'].'">';
                echo '<input type="button" value="-" onclick="$(\'#edu'.htmlentities($school['rank']).'\').remove();return false;">';
                echo '</p>';
                echo '<p>School: <input type="text" size="60" name="edu_school'.htmlentities($school['rank']).'" class="school" value="'.htmlentities($school['name']).'">';
                echo '</p>';
                echo '</div>';
            }
        }
        echo '</div>';

        
        echo '<p>Position: <input type="submit" id="addPos" value="+"></p>';
        echo '<div id="position_fields">';
        if(count($positions) > 0){
            foreach($positions as $position){
                echo '<div id="position'.htmlentities($position['rank']).'">';
                echo '<p>Year: <input type="text"  name="year'.$position['rank'].'" value="'.htmlentities($position['year']).'">';
                echo '<input type="button" value="-" onclick="$(position'.htmlentities($position['rank']).').remove();return false;">';
                echo '</p>';
                echo '<p>Description:<br> <textarea rows="10" cols="80" name="desc'.$position['rank'].'" size="40">'.htmlentities($position['description']).'</textarea>';
                echo '</p>';
                echo '</div>';
            }
        }
        echo '</div>';
    ?>
<p><input type="submit" name = "add" value="Save"/>
<input type="submit" name="cancel" value="Cancel"></p>
</form>
</div>
   <script>
    var count = $("#position_fields div").length;
    var count2 = $("#edu_fields div").length;
    console.log('count2  is: '+count2);
    countPos = count;
    countEdu = count2;

    // http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
    $(document).ready(function(){
        window.console && console.log('Document ready called');
        window.console && console.log('Total Count '+ count);
        $('#addPos').click(function(event){
            // http://api.jquery.com/event.preventdefault/
            event.preventDefault();
            if ( countPos >= 9 ) {
                alert("Maximum of nine position entries exceeded");
                return;
            }
            countPos++;
            window.console && console.log("Adding position "+countPos);
            $('#position_fields').append(
                '<div id="position'+countPos+'"> \
                <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
                <input type="button" value="-" \
                    onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
                 <p>Description:<br> <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea></p>\
                </div>');
        });

        $('#addEdu').click(function(event){
        event.preventDefault();
        if ( countEdu >= 9 ) {
            alert("Maximum of nine education entries exceeded");
            return;
        }
        countEdu++;
        window.console && console.log("Adding education "+countEdu);

        // // Grab some HTML with hot spots and insert into the DOM
        // var source  = $("#edu-template").html();
        // $('#edu_fields').append(source.replace(/@COUNT@/g,countEdu));

        // Tried this not working properly too
        $('#edu_fields').append(
            '<div id="edu'+countEdu+'"> \
            <p>Year: <input type="text" name="edu_year'+countEdu+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#edu'+countEdu+'\').remove();return false;"></p> \
                <p>School: <input type="text" size="80" name="edu_school'+countEdu+'" class="school" value="" /></p>\
            </div>');

        // Add the even handler to the new ones
        $('.school').autocomplete({
            source: "school.php"
        });

    });

    $('.school').autocomplete({
        source: "school.php"
    });

    });
    </script>
    <!-- <script id="edu-template" type="text">
        <div id="edu@COUNT@">
            <p>Year: <input type="text" name="edu_year@COUNT@" value="" />
            <input type="button" value="-" onclick="$('#edu@COUNT@').remove();return false;"><br>
            <p>School: <input type="text" size="80" name="edu_school@COUNT@" class="school" value="" />
            </p>
        </div>
    </script> -->
</body>
</html>