 <?php
    require_once "pdo.php";
    session_start();

    // Content type enables the Jquery script read the files
    header('Content-Type: application/json; charset=utf-8');
    $stmt = $pdo->prepare('SELECT institution_id, name FROM Institution WHERE name LIKE :prefix');
    $stmt->execute(array( ':prefix' => $_REQUEST['term']."%"));
    $retval = array();
    while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) {
        $retval[] = $row['name'];
    }
    echo(json_encode($retval, JSON_PRETTY_PRINT));

