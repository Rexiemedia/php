<?php
require_once "pdo.php";
require_once "utility.php";

session_start();
checkAccess();

// If the user requested logout go back to index.php
if ( isset($_POST['cancle']) ) {
    header('Location: index.php');
    return;
}

if ( isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email']) && isset($_POST['headline']) && isset($_POST['summary'])
     && isset($_POST['user_id'])) {

    // Data validation
    $msg = validateProfile();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
         header("Location: add.php");
        return;
    }

    //  Validate Education
    $msg = validateEdu();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
         header("Location: add.php");
        return;
    } 

    //  Validate Positions
    $msg = validatePos();
    if(is_string($msg)){
        $_SESSION['error'] = $msg;
         header("Location: add.php");
        return;
    } 

    $stmt = $pdo->prepare('INSERT INTO Profile
        (user_id, first_name, last_name, email, headline, summary)
        VALUES ( :uid, :fn, :ln, :em, :he, :su)');

    $stmt->execute(array(
        ':uid' => $_SESSION['user_id'],
        ':fn' => $_POST['first_name'],
        ':ln' => $_POST['last_name'],
        ':em' => $_POST['email'],
        ':he' => $_POST['headline'],
        ':su' => $_POST['summary'])
    );
    $_REQUEST['profile_id'] = $pdo->lastInsertId();
    
    insertPositions($pdo, $_REQUEST['profile_id']);

    insetEducation($pdo, $_REQUEST['profile_id']);

    $_SESSION['success'] = 'Profile added';
    header( 'Location: index.php' ) ;
    return;
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Rex Ovie Otavotoma</title>
<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Adding Profile for UMSI</h1>

 <?php flashErr(); ?>

<form  method="post">
<p>First Name:
     <?php
        if(isset($_GET['first_name'])) {
            $first_name = htmlentities($_GET['first_name']);
            echo '<input type="text" name="first_name" size="40" value="'.$first_name.'">';
        }
        else{  
            echo '<input type="text" name="first_name" size="40">';
        }
    ?>
</p>
<p>Last Name:
    <?php
        if(isset($_GET['last_name'])) {
            $last_name = htmlentities($_GET['last_name']);
            echo '<input type="text" name="last_name" size="40" value="'.$last_name.'">'; 
        }
        else{ 
            echo '<input type="text" name="last_name" size="40">'; 
        }
    ?>
</p>
<p>Email:
    <?php
        if(isset($_GET['email'])) {
            $year = htmlentities($_GET['email']);
            echo '<input type="text" name="email" size="40" value="'.$email.'">';
        }
        else{
            echo '<input type="text" name="email" size="40">';
        }
    ?>
</p>
<p>Headline: <br>
     <?php
        if(isset($_GET['headline'])) {
            $mileage = htmlentities($_GET['headline']);
            echo '<input type="text" name="headline" size="40" value="'.$headline.'">';
        }
        else{
            echo '<input type="text" name="headline" size="40">';
        }
    ?>
</p>
<p>Summary: <br>
     <?php
        if(isset($_GET['summary'])) {
            $mileage = htmlentities($_GET['summary']);
            echo '<textarea rows="10" cols="40" name="summary" size="40"> '.$summary.'"</textarea>';
        }
        else{
            echo '<textarea rows="8" cols="60" name="summary" size="40"></textarea>';
        }
    ?>
</p>

<p>
Education: <input type="submit" id="addEdu" value="+">
<div id="edu_fields">
</div>
</p>
<p>Position: <input type="submit" id="addPos" value="+"></p>
<div id="position_fields">
</div>
<input type="hidden" name="user_id" value="<?= $user_id ?>">
<input type="submit" value="Add" >
<input type="submit" name="cancle" value="Cancle">
</form>
</div>
<script>
countPos = 0;
countEdu = 0;

// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
    window.console && console.log('Document ready called');

    $('#addPos').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countPos >= 9 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countPos++;
        window.console && console.log("Adding position "+countPos);
        $('#position_fields').append(
            '<div id="position'+countPos+'"> \
            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
            <input type="button" value="-" onclick="$(\'#position'+countPos+'\').remove();return false;"></p>\
            <p>Summary: <br><textarea name="desc'+countPos+'" rows="8" cols="80"></textarea></p>\
            </div>');
    });

    $('#addEdu').click(function(event){
        event.preventDefault();
        if ( countEdu >= 9 ) {
            alert("Maximum of nine education entries exceeded");
            return;
        }
        countEdu++;
        window.console && console.log("Adding education "+countEdu);

        $('#edu_fields').append(
            '<div id="edu'+countEdu+'"> \
            <p>Year: <input type="text" name="edu_year'+countEdu+'" value="" /> \
            <input type="button" value="-" onclick="$(\'#edu'+countEdu+'\').remove();return false;"><br>\
            <p>School: <input type="text" size="60" name="edu_school'+countEdu+'" class="school" value="" />\
            </p></div>'
        );

        $('.school').autocomplete({
            source: "school.php"
        });

    });

});
</script>
</body>
</html>