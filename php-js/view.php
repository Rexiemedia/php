<?php
require_once "pdo.php";
require_once "utility.php";
session_start();

// retrieving the data
$stmt = $pdo->prepare("SELECT * FROM Profile WHERE profile.profile_id = :xyz;");
$stmt->execute(array(":xyz" => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value for profile_id';
    header( 'Location: index.php' ) ;
    return;
}

flashErr();

    $fn = htmlentities($row['first_name']);
    $ln = htmlentities($row['last_name']);
    $em = htmlentities($row['email']);
    $hl = htmlentities($row['headline']);
    $sm = htmlentities($row['summary']);
    $profile_id = htmlentities($_REQUEST['profile_id']);

    $positions =  loadPos($pdo, $profile_id);
    $schools =  loadEdu($pdo, $profile_id);
    echo $_SESSION['institution_id'];
?>

<!DOCTYPE html>
<html>
<head>
<title>Rex Ovie Otavotoma profile View</title>

<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Profile information</h1>
<input type="hidden" name="profile_id" value="<?= $profile_id ?>">
<p>First Name: <?= $fn ?></p>
<p>Last Name: <?= $ln ?></p>
<p>Email: <?= $em ?></p>
<p>Headline:<br/>
 <?= $hl ?></p>
<p>Summary:<br/>
<?= $sm ?>
</p>
<?php
    echo '<p>Education:';
     if(count($schools) > 0){
            foreach($schools as $school){
                echo '<ul>';
                echo '<li>'.htmlentities($school['year']).' : '.htmlentities($school['name']).'</li></ul>';
            }
        }
    echo '</p>';
    echo '<p>Positions:';
     if(count($positions) > 0){
            foreach($positions as $position){
                echo '<ul>';
                echo '<li>'.htmlentities($position['year']).' : '.htmlentities($position['description']).'</li></ul>';
            }
        }
    echo '</p>';
?>
<a href="index.php">Done</a>
</div>
    <script>
        function partyAnimal (){
            this.x = 1;
            this.party = function (){
                this.x = this.x * 3;
                console.log("So far "+this.x);
            }
        }

        newVal = new partyAnimal();
        newVal.party(); 
        // every time newVal(); runs its 3 9 27 ...

        
        function partyAnimal (nam){
            this.x = 1;
            this.name = nam; // this is a constructor
            console.log("Built  "+nam); // run time Built Sally and built Jim because of the names passed it below
            this.party = function (){
                this.x = this.x * 3;
                console.log(nam+" = "+this.x);
            }
        }

        s= new partyAnimal("Sally");
        s.party(); // Sally = 3  which is 3 X 1

        j = new partyAnimal("Jim");
        j.party(); // Jim = 3
        s.party(); // Sally = 9 because sally is being called second time
    </script>
</body>
</html>
