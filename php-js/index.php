
<?php
require_once "pdo.php";
require_once "utility.php";
session_start();

// If the user requested logout go back to index.php
if ( isset($_POST['logout']) ) {
    header('Location: index.php');
    return;
}

// retrieving the data
$stmt = $pdo->query("SELECT first_name, last_name, headline,  profile_id FROM Profile");
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
// $i = 0;
// while ( $row = $results->fetch(PDO::FETCH_ASSOC) ) {
//     yield $row[$i];
//     $i++;
// }
?>

<!DOCTYPE html>
<html>
<head>
<title>Rex Ovie Otavotoma Resume Registry</title>
<style>
tr th {
    padding: 10px ;
    border: 1px solid black;
    text-align: center;
    width: 160px;
    height: 30px;
}
td {
    padding: 20px ;
    border: 1px solid black;
    text-align: center;
    width: 160px;
    height: 30px;
}
a{
    padding: 15px;
}
</style>
<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Rex Ovie Otavotoma Resume Registry</h1>
 <p>
    <?php
        if ( isset($_SESSION["name"]) ) {
            echo '<a href="logout.php" name="logout">Logout</a>';
        }
        if( !isset($_SESSION["name"]) ){
            echo '<a href="login.php">Please log in</a>';
        }
        ?>
        </p>
        
    <?php
        flashSuccess();
        flashErr(); 
    ?>
 <table border="1">
    <?php
     echo "<tr><th>Name</th><th>Headline</th>";
        if ( isset($_SESSION["name"]) ){
     echo "<th>Action</th></tr>";
        }

        foreach ( $rows as $row ) {
            echo "<tr><td>";
            echo('<a href="view.php?profile_id='.$row['profile_id'].'">'.$row['first_name'].' '.$row['last_name'].'"</a>');
            echo("</td><td>");
            echo($row['headline']);
            echo("</td>");
            if ( isset($_SESSION["name"]) ){
                 echo "<td>";
                echo('<a href="edit.php?profile_id='.$row['profile_id'].'">Edit</a>');
                echo('<a href="delete.php?profile_id='.$row['profile_id'].'">Delete</a>');
                echo("</td>");
            }
            echo("</tr>\n");
        }
    ?>
 </table>

<p><strong>Note:</strong> Your implementation should retain data across multiple logout/login sessions. <br>This sample implementation clears all its data periodically - which you should not do in your implementation.</p>
<?php
    if ( isset($_SESSION["name"]) && isset($_SESSION["user_id"])){
        echo '<p><a href="add.php">Add New Entry</a><p>';
    }
?>
<!--<script async src="script.js"></script>-->
 
    <script type="text/javascript">
    // Load CSS
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
</script>
 
</div>
</body>
</html>
