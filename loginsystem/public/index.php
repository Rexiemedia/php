<?php

    session_start();
    include_once '../loginapp/header.php';
    include_once '../loginapp/pdo/pdo.php';
?>
<main class="container index-page index-image">
    <div class="row">
        <div class="col-12 col-sm-10 mx-auto mt-5 pt-5">
            <h2>Home</h2>
            <?php 
            if (isset($_SESSION['success'])) {
                echo '<h4>' . $_SESSION['success'] . '!</h4>';
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['error'])) {
                echo '<h4>' . $_SESSION['error'] . '!</h4>';
                unset($_SESSION['error']);
            }
            if (isset($_SESSION['verified'])) {
                echo '<h4>Welcom ' . $_SESSION['verified'] . '!</h4>';
            }
            ?>
        </div>
    </div>
</main>
<?php
    include_once '../loginapp/footer.php';
?>