<?php

// See the "errors" folder for details... instead of outputting it for users
try{
    $pdo = new PDO(
        'mysql:host=localhost;port=3306;dbname=loginsystem',
        'rexie',
        'Alpha'
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    error_log('Connection Error' . $e->getLine() . $e->getFile() . $e->getMessage());
    echo '<div class="alert alert-danger">Error Please contact Support</div>';
}
