<?php
    session_start();
    include_once 'header.php';
    include_once './pdo/pdo.php';

if (!isset($_SESSION['verified'])) {
    die('ACCESS DENIED');
    exit();
    return;
}

$stmt = $pdo->prepare("SELECT user_first  FROM users where user_id = :xyz");
$stmt->execute(array(":xyz" => $_SESSION['user_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ($row === false) {
    $_SESSION['error'] = 'Bad value of this User';
    header('Location: dashboard.php');
    return;
}

if (isset($_POST['delete']) && isset($_SESSION['user_id'])) {
    $sql = "DELETE FROM users WHERE user_id = :zip";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(':zip' => $_SESSION['user_id']));
    $_SESSION['success'] = 'Profile deleted';

    unset($_SESSION['verified']);
    unset($_SESSION['user_uid']);
    session_destroy();
    header('Location: index.php');
    return;
}

// Guardian: first_name sure that profile_id is present
if (!isset($_SESSION['user_id'])) {
    $_SESSION['error'] = "Missing profile_id";
    header('Location: index.php');
    return;
}
?>
<main class="container index-page">
    <div class="row">
        <div class="col-12 col-sm-10 mx-auto mt-5 pt-5">
            <p>Confirm: Deleting <?= htmlentities($row['first_name']) ?><br>
            <?= htmlentities($row['last_name']) ?></p>

        <form method="post">
            <input type="hidden" name="user_id" value="<?= $row['user_id'] ?>">
            <input type="submit" value="Delete" name="delete">
            <a href="index.php">Cancel</a>
        </form>
    </div>
    </div>
</main>