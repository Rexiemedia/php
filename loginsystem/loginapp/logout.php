<?php
    session_start();
    unset($_SESSION['verified']);
    unset($_SESSION['user_uid']);
    session_destroy();
    header("Location: index.php");