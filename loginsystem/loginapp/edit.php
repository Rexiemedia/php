 <?php
    session_start();
    include_once 'header.php';
    include_once './pdo/pdo.php';

    // Guardian:  user  is present
    if (!isset($_SESSION['verified'])) {
        exit();
        return;
    }

    //Getting the data fro user update
if (isset($_SESSION['user_id']) && isset($_SESSION['verified'])) {

    $user_id = htmlentities($_SESSION['user_id']);

    $stmt = $pdo->prepare('SELECT * FROM users WHERE user_id = :user_id');
    $stmt->execute(array(':user_id' => $user_id));
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    $fn = htmlentities($result['user_first']);
    $ln = htmlentities($result['user_last']);
    $em = htmlentities($result['user_email']);
    $uid = htmlentities($result['user_uid']);
    $pwd = htmlentities($result['user_pwd']);

}

if (isset($_POST['first']) && isset($_POST['last']) && isset($_POST['email']) && isset($_POST['uid']) && isset($_POST['pwd'])) {

    $first = htmlentities($_POST['first']);
    $last = htmlentities($_POST['last']);
    $email = htmlentities($_POST['email']);
    $uid = htmlentities($_POST['uid']);
    $pwd = htmlentities($_POST['pwd']);

        // Error handlers
        // Check for empty fields
    if (empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd)) {
        $_SESSION['error'] = 'All Fileds required';
        header("Location: signup.php?signup=empty");
        return;
    } else {
            // // Check for bad character fields
        if (strlen($_POST['first']) < 2 || strlen($_POST['last']) < 2) {
            $_SESSION['error'] = 'First and Last names must be 3 character long';
            header("Location: signup.php?signup=empty");
            return;
        }
        if (strpos($_POST['email'], '@') === false) {
            $_SESSION['error'] = 'Email address must contain @';
            header("Location: signup.php?signup=empty");
            return;
        }
         else {
            $options = [
                'cost' => 11,
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
            ];
            $hashedPdw = password_hash($pwd, PASSWORD_BCRYPT, $options);

                // Update User
            $stmt = $pdo->prepare('UPDATE users SET user_first = :usrFn,
            user_last = :usrLn, user_email = :usrEm,
            user_uid = :usrUid, user_pwd = :usrPwd
            WHERE user_id = :usr_id');

            $stmt->execute(array(
                ':usr_id' => $user_id,
                ':usrFn' => $first,
                ':usrLn' => $last,
                ':usrEm' => $email,
                ':usrUid' => $uid,
                ':usrPwd' => $hashedPdw
            ));
        }
    }
    $_SESSION['success'] = 'Successfully Updated';
    unset($_SESSION['verified']);
    unset($_SESSION['user_uid']);
    session_destroy();
    header('Location: index.php');
    return;
}
?>

<div class="container mb-5 mt-5 index-page">
    <div class="row">
        <div class="col-12 col-md-8 mx-auto text-center">
            <form class="form-signin" method="POST">
                <h1 class="h3 mb-3 mt-5 font-weight-normal">Editing Detail..  <?= $fn ?></h1>
                <input type="hidden" name="profile_id" value="<?= $profile_id ?>">
                <label for="inputEmail" class="sr-only">First Name</label>
                <input type="text" id="first" name="first" class="form-control mb-1" value="<?= $fn ?>" placeholder="First Name" required autofocus>
                <label for="inputEmail" class="sr-only">Last Name</label>
                <input type="text" name="last" id="last" class="form-control mb-1" value="<?= $ln ?>" placeholder="Last Name" required autofocus>
                <label for="inputEmail" class="sr-only">Email Address</label>
                <input type="email" name="email" id="email" class="form-control mb-1" value="<?= $em ?>" placeholder="Email Address" required autofocus>
                <label for="inputEmail" class="sr-only">User Name</label>
                <input type="text" name="uid" id="uid" class="form-control mb-1" value="<?= $uid ?>" placeholder="User Name" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="pwd" id="pwd" class="form-control mb-1" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
            </form>
        </div>
    </div>
        <div class="row">
        <div class="col-12 col-md-8 mx-auto mt-2 mb-5">
            <?php
            if (isset($_SESSION['error'])) {
                echo '<h2 style="text-align: center; color: red;text-shadow: 2px 2px 2px  #000;"><strong>' . $_SESSION['error'] . '!</strong></h2>';
                unset($_SESSION['error']);
            }
            ?>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>