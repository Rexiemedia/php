 <footer class="page-footer pt-2">
    <div class="container">
      <div class="row>
        <div class="col-12 offset-l2">
          <h5 class="white-text">Links</h5>
          <ul style="list-style:none;">
            <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
            <li class="nav-item">
              <?php
                if (isset($_SESSION['verified'])) {
                  echo '<a class="grey-text text-lighten-3" href="dashboard.php">DashBoard</a>';
                }
              ?>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <p class="mt-2 mb-2 text-muted">© 2018 rexyMedia</p>
      </div>
    </div>
</footer>