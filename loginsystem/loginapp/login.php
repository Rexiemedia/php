<?php
    include_once './pdo/pdo.php';
    session_start();

if ( isset($_POST['uid']) && isset($_POST['pwd'])) {

    $uid = htmlentities($_POST['uid']);
    $pwd = htmlentities($_POST['pwd']);


    if( empty($uid) || empty($pwd) ) {
            $_SESSION['error'] = 'All Fileds required';
            header("Location: index.php?login=empty");
            return;
    }
            
    $stmt = $pdo->prepare('SELECT * FROM users WHERE user_uid = :uid OR user_email = :uid');
    $stmt->execute(array(':uid' => $uid));
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $hash = $result['user_pwd']; 

    if (password_verify($pwd, $hash)) { 
        $_SESSION['verified'] = $result['user_first'];
        $_SESSION['user_uid'] = $result['user_uid'];
        $_SESSION['user_id'] = $result['user_id'];
        header("Location: dashboard.php");
    } else { 
        $_SESSION['error'] = 'Invalid password.';
        header("Location: index.php");
    } 
 }