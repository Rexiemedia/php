 <?php
    session_start();
    include_once 'header.php';
    include_once './pdo/pdo.php';
?>
<div class="container mb-5 mt-5 index-page">
    <div class="row">
        <div class="col-12 col-md-8 mx-auto text-center">
        <form class="form-signin my-2 my-lg-0" method="POST" action="login.php">
        <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address / User Name</label>
        <input type="text" name="uid" placeholder="Username / e-mail" id="uid" class="form-control mb-2" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="pwd" id="pwd" class="form-control mb-2" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-8 mx-auto mt-2 mb-5">
            <?php
            if (isset($_SESSION['error'])) {
                echo '<h4>' . $_SESSION['error'] . '!</h4>';
                unset($_SESSION['error']);
            }
            ?>
       </div>
    </div>
</div>

<?php
    include_once 'footer.php';
?>
