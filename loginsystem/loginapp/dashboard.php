<?php
    session_start();
    include_once 'header.php';
    include_once './pdo/pdo.php';

    if (!isset($_SESSION['verified'])) {
        exit();
        return;
    }
?>

<main class="container index-page">
    <div class="row">
        <div class="col-12 col-sm-10 mx-auto mt-5 pt-5">
            <?php 
            if (isset($_SESSION['success'])) {
                echo '<h4>'.$_SESSION['success'].'!</h4>';
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['error'])) {
                echo '<h4>'.$_SESSION['error'].'!</h4>';
                unset($_SESSION['error']);
            }
            if (isset($_SESSION['verified'])) {
                echo '<h4>Welcom '.$_SESSION['verified'].'! To Your DashBoard</h4>';
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-10 mx-auto mt-5 pt-5">
            <div>
                <h3>Edith or Delete User</h3>
                <a href="edit.php?profile_id='.$_SESSION['user_id'].'">Edit</a>
                <a href="delete.php?profile_id='.$_SESSION['user_id'].'">Delete</a>
            </div>
        </div>
    </div>
</main>
<?php
include_once 'footer.php';
?>
