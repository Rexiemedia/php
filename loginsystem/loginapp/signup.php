 <?php
    session_start();
    include_once 'header.php';
    include_once './pdo/pdo.php';

    if ( isset($_POST['first']) && isset($_POST['last']) && isset($_POST['email']) && isset($_POST['uid']) && isset($_POST['pwd'])) {

        $first = htmlentities($_POST['first']);
        $last = htmlentities($_POST['last']);
        $email = htmlentities($_POST['email']);
        $uid = htmlentities($_POST['uid']);
        $pwd = htmlentities($_POST['pwd']);

        // Error handlers
        // Check for empty fields
        if(empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd) ) {
            $_SESSION['error'] = 'All Fileds required';
            header("Location: signup.php?signup=empty");
            return;
        }
        else {
            // // Check for bad character fields
            if(strlen($_POST['first']) < 2 ||  strlen($_POST['last']) < 2 ) {
                $_SESSION['error'] = 'First and Last names must be 3 character long';
                header("Location: signup.php?signup=empty");
                return;
            }
            if ( strpos($_POST['email'],'@') === false ) {
                $_SESSION['error'] = 'Email address must contain @';
                header("Location: signup.php?signup=empty");
                return;
            }
            
            $stmt = $pdo->prepare('SELECT * FROM users WHERE user_uid = :usrUid');
            $stmt->execute(array(':usrUid' => $uid));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $count = count($result);
            
            if($count > 0) {
                $_SESSION['error'] = 'User already Exist';
                header("Location: signup.php?signup=exist");
                return; 
            } else {
                // hash passwrod http://php.net/manual/en/function.password-hash.php
                $options = [
                    'cost' => 11,
                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
                ];
                $hashedPdw = password_hash($pwd, PASSWORD_BCRYPT, $options);

                // Inserting User
                $stmt = $pdo->prepare('INSERT INTO users ( user_first, user_last, user_email, user_uid, user_pwd)
                VALUES ( :usrFn, :usrLn, :usrEm, :usrUid, :usrPwd)');

                $stmt->execute(array(
                ':usrFn' => $first,
                ':usrLn' => $last,
                ':usrEm' => $email,
                ':usrUid' => $uid,
                ':usrPwd' => $hashedPdw)
                );
            }
        }
         $_SESSION['success'] = 'Signed Successfully! Please login';
         header("Location: index.php");
         return;
     }
?>

<div class="container mb-5 mt-5 pt-5 index-page">
    <div class="row">
        <div class="col-12 col-md-8 mx-auto text-center">
            <form class="form-signin" method="POST">
                <h1 class="h3 mb-3 font-weight-normal">Please SignUp</h1>
                <label for="inputEmail" class="sr-only">First Name</label>
                <input type="text" id="first" name="first" class="form-control mb-1" placeholder="First Name" required autofocus>
                <label for="inputEmail" class="sr-only">Last Name</label>
                <input type="text" name="last" id="last" class="form-control mb-1" placeholder="Last Name" required autofocus>
                <label for="inputEmail" class="sr-only">Email Address</label>
                <input type="email" name="email" id="email" class="form-control mb-1" placeholder="Email Address" required autofocus>
                <label for="inputEmail" class="sr-only">User Name</label>
                <input type="text" name="uid" id="uid" class="form-control mb-1" placeholder="User Name" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="pwd" id="pwd" class="form-control mb-1" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
            </form>
        </div>
    </div>
        <div class="row">
        <div class="col-12 col-md-8 mx-auto mt-2 mb-5">
            <?php
        if (isset($_SESSION['error'])) {
            echo '<h2 style="text-align: center; color: red;text-shadow: 2px 2px 2px  #000;"><strong>' . $_SESSION['error'] . '!</strong></h2>';
            unset($_SESSION['error']);
        }
        ?>
        </div>
    </div>
</div>

<?php
    include_once 'footer.php';
?>