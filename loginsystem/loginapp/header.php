<!DOCTYPE  html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>rexyMedia LoginSystem</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">    
    <link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark pl-5">
      <a class="navbar-brand mr-auto mr-lg-0 ml-5" href="#">rexyMedia Login-System</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto mr-5 ml-5">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
               <?php  
                    // If user is not logged in show this block
                if (isset($_SESSION['verified'])) {
                  echo '<a class="nav-link" href="dashboard.php">Dashboard</a>';
                }
                ?>
          </li>
        </ul>
        <?php  
                    // If user is not logged in show this block
        if (!isset($_SESSION['verified'])) {
            echo '<ul class="navbar-nav mr-auto ml-5 pl-5">';
            echo '<li class="nav-item mr-3">';
            echo '<a class="nav-link" href="~/loginapp/signup.phpp">Login</a>';
            echo '</li>';
            echo '<li class="nav-item">';
            echo '<a class="nav-link" href="signup.php">Sign Up</a>';
            echo '</li>';
            echo '</ul>';
        }  
                    // If user show this block
        if (isset($_SESSION['verified'])) {
            echo '<ul class="navbar-nav mr-auto  ml-5 pl-5"><li class="nav-item">';
            echo '<a class="nav-link ml-5 pl-5" href="logout.php">Log Out</a>';
            echo '</li></ul>';
        }
        ?>
      </div>
    </nav>
</header>