<?php
    session_start();
    include_once 'header.php';
?>
<section class="main-container">
    <div class="main-wrapper">
        <h2>Home</h2>
    <?php  
        if(isset($_SESSION['success'])){
           echo '<h4>'.$_SESSION['success'].'!</h4>'; 
            unset($_SESSION['success']);
        } 
        if(isset($_SESSION['error'])){
           echo '<h4>'.$_SESSION['error'].'!</h4>'; 
            unset($_SESSION['error']);
        } 
        if(isset($_SESSION['verified'])) {
            echo '<h4>Welcom '.$_SESSION['verified'].'!</h4>';
        }
   ?>

    </div>
</section>
<?php
    include_once 'footer.php';
?>