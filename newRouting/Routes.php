<?php

    Route::set('index.php', function () {
            // Accessing the About us class from classes
        Index::CreateView('Index');
    });

    Route::set('about-us', function(){
        // Accessing the About us class from classes
        AboutUs::CreateView('AboutUs');
    });

    Route::set('contact-us', function(){
        // Accessing the Contact us class from classes that was extend from Controllers
        ContactUs::CreateView('ContactUs');
    });
?>